package com.concretesolutions.test.githubjavarepositories.data.service;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public class ApiContractImpl implements ApiContract {

    public static final String BASE_URL = "https://api.github.com/";

    private Api api;

    public ApiContractImpl(){
        setupApi();
    }

    @Override
    public Api getApiInstance() {
        return api;
    }

    @Override
    public void setupApi() {
        HttpLoggingInterceptor interceptorHttp = new HttpLoggingInterceptor();
        interceptorHttp.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .cache(new Cache(new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString()), 8 * 1024 * 1024))
                .addInterceptor(interceptorHttp)
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(5, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        api = retrofit.create(Api.class);


    }
}
