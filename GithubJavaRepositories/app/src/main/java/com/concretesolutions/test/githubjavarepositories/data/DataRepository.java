package com.concretesolutions.test.githubjavarepositories.data;

import com.concretesolutions.test.githubjavarepositories.data.model.PullRequest;
import com.concretesolutions.test.githubjavarepositories.data.model.Repository;

import java.util.List;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public interface DataRepository {
    interface getRepoListOnFinishedListener {
        void onFinishedList(List<Repository> list);
    }

    void getRepositoryList(int page, getRepoListOnFinishedListener listener);

    interface getPullListOnFinishedListener{
        void onFinishedList(List<PullRequest> list);
    }

    void getPullRequestList(getPullListOnFinishedListener listener, String repoName,
                            String ownerName);
}