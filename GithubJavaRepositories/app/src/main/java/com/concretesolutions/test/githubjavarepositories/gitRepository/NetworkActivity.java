package com.concretesolutions.test.githubjavarepositories.gitRepository;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public abstract class NetworkActivity extends AppCompatActivity {

    private ReceiverConn receiverConn;

    @Override
    public void onResume(){
        super.onResume();
        receiverConn = new ReceiverConn();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiverConn, intentFilter);
    }

    @Override
    public void onPause(){
        super.onPause();
        unregisterReceiver(receiverConn);
    }

    public abstract void setupDownload();

    public class ReceiverConn extends BroadcastReceiver{

        boolean momentOne = true;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (momentOne){
                momentOne = false;
                return;
            }else if(ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                if (MainActivity.hasConnection(context)) {
                    setupDownload();
                }
            }
        }
    }
}
