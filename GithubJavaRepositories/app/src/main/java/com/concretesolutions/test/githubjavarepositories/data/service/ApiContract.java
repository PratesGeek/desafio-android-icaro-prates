package com.concretesolutions.test.githubjavarepositories.data.service;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public interface ApiContract {

    Api getApiInstance();

    void setupApi();
}
