package com.concretesolutions.test.githubjavarepositories.gitRepository;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.concretesolutions.test.githubjavarepositories.AlertDialogFragment;
import com.concretesolutions.test.githubjavarepositories.EndlessRecyclerViewScrollListener;
import com.concretesolutions.test.githubjavarepositories.R;
import com.concretesolutions.test.githubjavarepositories.data.DataRepositoryImpl;
import com.concretesolutions.test.githubjavarepositories.data.model.Repository;
import com.concretesolutions.test.githubjavarepositories.pullRequests.PullRequestActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends NetworkActivity implements MainContract.View {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String EXTRA_POSITION = "position";
    public static final String EXTRA_PAGE = "page";
    public static final String EXTRA_REPOSITORY = "repository";
    public static final String EXTRA_OWNER = "owner";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.no_item)
    TextView noRepoText;

    private MainContract.UserActionListener userActionListener;
    private List<Repository> repoList = new ArrayList<>();
    private RepoAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int currentPage = 1;
    private int lastVisiblePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("NGVL", "teste1::onCreate");
        setContentView(R.layout.activity_main);
        Log.i("NGVL", "teste2::onCreate");
        ButterKnife.bind(this);
        Log.i("NGVL", "teste3::onCreate");
        setSupportActionBar(toolbar);
        Log.i("NGVL", "teste4::onCreate");
        userActionListener = new MainPresenter(this, new DataRepositoryImpl());
        Log.i("NGVL", "teste5::onCreate");
        if (MainActivity.hasConnection(this)) {
            Log.i("NGVL", "teste6::onCreate");
            userActionListener.fetchRepoList(currentPage);
            Log.i("NGVL", "teste7::onCreate");
        } else {
            Log.i("NGVL", "teste8::onCreate");
            AlertDialogFragment alertDialogFragment =
                    AlertDialogFragment.newInstance(getResources().getString(R.string.network_msg));
            Log.i("NGVL", "teste9::onCreate");
            alertDialogFragment.show(getSupportFragmentManager(), "alert");
            Log.i("NGVL", "teste10::onCreate");
        }
        Log.i("NGVL", "teste11::onCreate");
        setupRecyclerView(lastVisiblePosition);
        Log.i("NGVL", "teste12::onCreate");

    }

    private void setupRecyclerView(int lastPosition) {
        Log.i("NGVL", "teste13::onCreate");
        adapter = new RepoAdapter(repoList, repoTouchListener);
        Log.i("NGVL", "teste14::onCreate");
        recyclerView.setAdapter(adapter);
        Log.i("NGVL", "teste15::onCreate");
        layoutManager = new LinearLayoutManager(getApplicationContext());
        Log.i("NGVL", "teste16::onCreate");
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        Log.i("NGVL", "teste17::onCreate");
        layoutManager.scrollToPosition(lastPosition);
        Log.i("NGVL", "teste18::onCreate");
        recyclerView.setLayoutManager(layoutManager);
        Log.i("NGVL", "teste19::onCreate");
        recyclerView.setHasFixedSize(true);
        Log.i("NGVL", "teste20::onCreate");
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {

            @Override
            public void onLoadMore(int page) {
                Log.i("NGVL", "teste21::onCreate");
                currentPage = page;
                Log.i("NGVL", "teste22::onCreate");
                userActionListener.fetchRepoList(currentPage);
                Log.i("NGVL", "teste23::onCreate");
            }

        });
        Log.i("NGVL", "teste24::onCreate");
    }



    public static boolean hasConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null){
            return networkInfo.isConnected();
        }else{
            return false;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showRepoList(List<Repository> list) {
        repoList.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showNoRepoText() {
        recyclerView.setVisibility(View.GONE);
        noRepoText.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPullRequestActivity(Repository repo) {
        if (repo != null) {
            Intent intent = new Intent(this, PullRequestActivity.class);
            intent.putExtra(EXTRA_OWNER, repo.owner.name);
            intent.putExtra(EXTRA_REPOSITORY, repo.name);
            startActivity(intent);
        }
    }

    @Override
    public void setupDownload() {
        userActionListener = new MainPresenter(this, new DataRepositoryImpl());
        userActionListener.fetchRepoList(1);
    }

    public interface RepoTouchListener {
        void onRepoTouch(Repository touchedRepo);
    }

    RepoTouchListener repoTouchListener = new RepoTouchListener() {
        @Override
        public void onRepoTouch(Repository touchedRepo) {
            userActionListener.openRepoPullRequests(touchedRepo);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(MainActivity.EXTRA_POSITION, layoutManager.findLastVisibleItemPosition());
        outState.putInt(MainActivity.EXTRA_PAGE, currentPage);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        lastVisiblePosition = savedInstanceState.getInt(MainActivity.EXTRA_POSITION);
        currentPage = savedInstanceState.getInt(MainActivity.EXTRA_PAGE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setupRecyclerView(layoutManager.findLastVisibleItemPosition());
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setupRecyclerView(layoutManager.findLastVisibleItemPosition());
        }
    }

}