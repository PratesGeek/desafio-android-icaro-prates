package com.concretesolutions.test.githubjavarepositories.gitRepository;

import android.support.annotation.NonNull;

import com.concretesolutions.test.githubjavarepositories.data.model.Repository;

import java.util.List;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public class MainContract {

    interface View{

        void showProgress();

        void hideProgress();

        void showRepoList(List<Repository> list);

        void showPullRequestActivity(Repository repo);

        void showNoRepoText();

    }

    interface UserActionListener{

        void fetchRepoList(int page);

        void openRepoPullRequests(@NonNull Repository requestedRepository);

    }
}
