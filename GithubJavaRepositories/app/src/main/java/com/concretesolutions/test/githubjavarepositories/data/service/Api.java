package com.concretesolutions.test.githubjavarepositories.data.service;

import com.concretesolutions.test.githubjavarepositories.data.model.Model;
import com.concretesolutions.test.githubjavarepositories.data.model.PullRequest;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public interface Api {

    @GET("/search/repositories")
    Call<Model> getRepositoryList(@QueryMap Map<String, String> query);

    @GET("repos/{owner}/{repository}/pulls")
    Call<List<PullRequest>> getPullRequestList(@Path("owner") String owner,
                                               @Path("repository") String repo);

}
