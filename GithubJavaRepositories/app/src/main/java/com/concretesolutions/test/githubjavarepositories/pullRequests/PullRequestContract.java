package com.concretesolutions.test.githubjavarepositories.pullRequests;

import com.concretesolutions.test.githubjavarepositories.data.model.PullRequest;

import java.util.List;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public class PullRequestContract {

    interface View{

        void showProgress();

        void hideProgress();

        void showPullList(List<PullRequest> list);

        void showNoPullText();

    }

    interface UserActionListener{

        void fetchPullList(String repoName, String ownerName);

    }
}
