package com.concretesolutions.test.githubjavarepositories.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Icaro Prates on 10/03/2017.
 */

public class Model {

    @SerializedName("items")
    public List<Repository> repositoryList = new ArrayList<>();
}
